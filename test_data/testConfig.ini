################################################################################
#
# Configuration file for SMRF v0.3.0-1-gbbd34f3
# Date generated: 2017-09-08

# For details on configuration file syntax see:
# https://docs.python.org/2/library/configparser.html
#
# For more SMRF related help see:
# http://smrf.readthedocs.io/en/latest/


################################################################################
# Files for DEM and vegetation
################################################################################

[topo]
type:                          netcdf    
filename:                      ../test_data/topo/topo.nc
basin_lat:                     43.8639   
basin_lon:                     -115.3333 


################################################################################
# Dates to run model
################################################################################

[time]
time_step:                     60        
start_date:                    2008-12-02 12:00
end_date:                      2008-12-02 18:00
time_zone:                     UTC       


################################################################################
# Stations to use
################################################################################

[stations]
stations:                      ATAI1, BOII, BNRI1, VNNI1, TRMI1, BOGI1, TR216


################################################################################
# CSV data files
################################################################################

[csv]
metadata:                      ../test_data/stationData/ta_metadata.csv
air_temp:                      ../test_data/stationData/final_air_temp.csv
vapor_pressure:                ../test_data/stationData/final_vp.csv
precip:                        ../test_data/stationData/final_precip.csv
wind_speed:                    ../test_data/stationData/final_wind_speed.csv
wind_direction:                ../test_data/stationData/final_wind_dir.csv
cloud_factor:                  ../test_data/stationData/final_cf.csv


################################################################################
# Air temperature distribution
################################################################################

[air_temp]
stations:                      ATAI1, BNRI1, VNNI1, TRMI1, BOGI1, TR216
distribution:                  idw       
detrend:                       True      
slope:                         -1        
power:                         2         
min:                           -73.0     
dk_nthreads:                   1         
max:                           47.0      
mask:                          False     
method:                        linear    


################################################################################
# Vapor pressure distribution
################################################################################

[vapor_pressure]
stations:                      BNRI1, BOGI1, ATAI1, TR216
distribution:                  idw       
detrend:                       True      
slope:                         -1        
tolerance:                     0.01      
nthreads:                      1         
power:                         2         
min:                           10.0      
dk_nthreads:                   1         
max:                           5000.0    
mask:                          False     
method:                        linear    


################################################################################
# Wind speed and wind direction distribution
################################################################################

[wind]
stations:                      TR216, VNNI1, ATAI1
distribution:                  idw       
detrend:                       False     
maxus_netcdf:                  ../test_data/topo/maxus.nc
tr216:                         0         
vnni1:                         3.0       
station_default:               11.4      
peak:                          TR216     
veg_default:                   11.4      
veg_41:                        11.4      
veg_42:                        11.4      
veg_43:                        3.3       
reduction_factor:              0.7       
slope:                         -1        
nthreads:                      1         
power:                         2         
dk_nthreads:                   1         
max:                           35.0      
min:                           0.0       
mask:                          False     
method:                        linear    


################################################################################
# Precipitation distribution
################################################################################

[precip]
stations:                      BNRI1, BOGI1, ATAI1, TRMI1, VNNI1
distribution:                  dk        
slope:                         1         
dk_nthreads:                   2         
regression_method:             1         
storm_mass_threshold:          0.1       
time_steps_to_end_storms:      3         
nasde_model:                   susong1999
nthreads:                      1         
power:                         2         
min:                           0         
max:                           None      
mask:                          False     
detrend:                       False     
method:                        linear    
storm_days_restart:            None      


################################################################################
# Albedo distribution
################################################################################

[albedo]
grain_size:                    300       
max_grain:                     2000      
dirt:                          2.0       
power:                         2         
min:                           0.0       
max:                           1.0       
mask:                          False     
method:                        linear    
decay_method:                  hardy2000 
litter_albedo:                 0.2       
litter_default:                0.003     
litter_41:                     0.006     
litter_42:                     0.006     
litter_43:                     0.003     
veg_43:                        0.25      
veg_42:                        0.36      
veg_41:                        0.36      
veg_default:                   0.25      
decay_power:                   0.714     


################################################################################
# Solar radiation distribution
################################################################################

[solar]
stations:                      BOII, BNRI1, TR216
clear_opt_depth:               100       
clear_tau:                     0.2       
clear_omega:                   0.85      
clear_gamma:                   0.3       
distribution:                  idw       
slope:                         -1        
nthreads:                      1         
power:                         2         
min:                           0.0       
mask:                          False     
dk_nthreads:                   1         
detrend:                       False     
max:                           800.0     
method:                        linear    


################################################################################
# Thermal radiation distribution
################################################################################

[thermal]
nthreads:                      4         
correct_cloud:                 True      
correct_veg:                   False     
cloud_method:                  garren2005
min:                           0.0       
max:                           600.0     
correct_terrain:               True      
method:                        marks1979 


################################################################################
#  Soil temperature
################################################################################

[soil_temp]
temp:                          -2.5      


################################################################################
# Output variables
################################################################################

[output]
frequency:                     1         
out_location:                  ~/Desktop/test_output
variables:                     thermal, air_temp, vapor_pressure, wind_speed, net_solar, precip, percent_snow, snow_density, dew_point
file_type:                     netcdf    
input_backup:                  True      


################################################################################
# Logging
################################################################################

[logging]
log_level:                     debug     


################################################################################
# System variables
################################################################################

[system]
time_out:                      30.0      
temp_dir:                      ../test_data
threading:                     True      
max_values:                    1         
