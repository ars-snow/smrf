# Spatial Modeling for Resources Framework V 0.3.0

Spatial Modeling for Resources Framework (SMRF) was developed by Dr. Scott Havens at
the USDA Agricultural Research Service (ARS) in Boise, ID. SMRF was designed to
increase the flexibility of taking measured weather data and distributing
the point measurements across a watershed. SMRF was developed to be used as an
operational or research framework, where ease of use, efficiency, and ability to
run in near real time are high priorities.


#### Usage 
Read the full documentation for [SMRF](https://smrf.readthedocs.io) including up to
date installation instructions.
