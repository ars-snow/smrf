smrf package
============

.. toctree::

    smrf.data
    smrf.distribute
    smrf.envphys
    smrf.framework
    smrf.ipw
    smrf.model
    smrf.output
    smrf.spatial
    smrf.utils

Module contents
---------------

.. automodule:: smrf
    :members:
    :undoc-members:
    :show-inheritance:
